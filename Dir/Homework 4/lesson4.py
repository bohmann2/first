from itertools import zip_longest

def convert_set_to_dict(my_set, val):
    my_dict = {}
    for i, k in enumerate(my_set):
        my_dict[k] = i ** 2

    val_in_keys = True if val in my_dict.keys() else False
    val_in_vals = True if val in my_dict.values() else False
    return my_dict, val_in_keys, val_in_vals

def test():
    print(list(zip("dfghhj")))


def zip_name_mark(name_list, grade_list):
    zip_longest(name_list, grade_list, fillvalue = "!!!")


def main():
#    my_set = {1,2,"abc", True}
#    convert_set_to_dict(my_set, True)

    list1 = ['abc', 'cde', 'fff']
    list2 = [3,4,5]
    list3 = zip_name_mark(list1, list2)

    print(list3)



if __name__ == '__main__':
    main()