# -*- coding: utf-8 -*-
# Функции работы веб драйвера покупка, продажа, навигация по сайту.

import os
import time
import string
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ActionChains

import settings

delay = 10 # seconds

driver = webdriver.Chrome("chromedriver.exe")
driver.set_window_size(900, 1020)
#driver.implicitly_wait(10) # seconds


sell_url = r'https://steamcommunity.com/INSERTIDHERE/inventory?modal=1&market=1#'
market_url = r'https://steamcommunity.com/market/'
main_url = r'https://steamcommunity.com/'
page_ascending_sort = '#pID_name_asc'
page_popular_sort = '#pID_popular_desc'

collection_csgo_url = r'https://steamcommunity.com/market/search?q=&category_730_ItemSet%5B%5D=INSERTTAGHERE&category_730_ProPlayer%5B%5D=any&category_730_StickerCapsule%5B%5D=any&category_730_TournamentTeam%5B%5D=any&category_730_Weapon%5B%5D=any&appid=730'
collection_artifact_url = r'https://steamcommunity.com/market/search?q=&appid=583950'
collection_dota_url = r'https://steamcommunity.com/market/search?q=&appid=570'
collection_tf_url = r'https://steamcommunity.com/market/search?q=&appid=440'

bought_items = 0
bought_items_total_sum = 0

# Получение целого числа из строки, убирая запятые, точки..
def get_int_from_str(string):
    temp_string = filter(lambda x: x in '0123456789', string)
    int_num = int(temp_string)
    return int_num


# Получение плавающего числа из строки, убирая все кроме, точек и чисел.
def get_float_from_str(string):
    temp_string = string.replace(".", "")
    temp_string = temp_string.replace(",", ".")
    temp_string = filter(lambda x: x in '0123456789.', temp_string)
    float_num = float(temp_string)
    return float_num


# Клик по элементу используя какой-то перформ
def click_on_button_perform(xpath, def_delay = delay):
    try:
        btn = WebDriverWait(driver, def_delay).until(EC.presence_of_element_located((By.XPATH, xpath)))
        driver.execute_script("(arguments[0]).click();", btn)
        actions = ActionChains(driver)
        actions.move_to_element(btn).click().perform()
        time.sleep(0.1)
    except:
        raise


# Клик по элементу используя скрипт
def click_on_button_scr(xpath, def_delay = delay):
    try:
        btn = WebDriverWait(driver, def_delay).until(EC.presence_of_element_located((By.XPATH, xpath)))
        driver.execute_script("(arguments[0]).click();", btn)
        time.sleep(0.1)
    except:
        raise

# Клик по элементу используя скрипт
def click_on_element(elem):
    try:
        elem.click()
        time.sleep(0.1)
    except:
        print("Could not click on element")
        raise

# Получение элементов с задержкой
def find_item_by_id(id, def_delay = delay):
    elem = WebDriverWait(driver, def_delay).until(EC.presence_of_element_located((By.ID, id)))
    return elem


def find_item_by_class_name(className, def_delay = delay):
    elem = WebDriverWait(driver, def_delay).until(EC.presence_of_element_located((By.CLASS_NAME, className)))
    return elem


def find_item_by_xpath(xpath, def_delay = delay):
    elem = WebDriverWait(driver, def_delay).until(EC.presence_of_element_located((By.XPATH, xpath)))
    return elem


# Открытие ссылки в новом окне
def open_link_in_new_window(ref):
    actions = ActionChains(driver)
    time.sleep(0.2)
    actions.key_down(Keys.CONTROL).click(ref).key_up(Keys.CONTROL).perform()
    time.sleep(0.2)
    driver.switch_to.window(driver.window_handles[-1])
    time.sleep(0.2)

def open_link_in_new_window_by_url(url):
    driver.execute_script("window.open("'"' + url + '"'");")
    time.sleep(0.3)
    driver.switch_to.window(driver.window_handles[-1])
    time.sleep(0.5)


# Закрытие окна и переключение назад
def close_link():

    tabs = len(driver.window_handles)
    if tabs == 1:
        print("Tabs are already 1!")
        return

    driver.close()
    time.sleep(0.2)
    driver.switch_to.window(driver.window_handles[0])
    time.sleep(0.2)



    
# Печать текста в строку бокс
def print_in_box(elem, str):
    elem.clear()
    time.sleep(0.05)
    elem.send_keys(str)
    time.sleep(0.3)
    elem.send_keys(Keys.RETURN)
    time.sleep(0.3)
    
# Загрузка страницы с задержкой
def get_url(url, def_wait=2.5):
    driver.get(url)
    time.sleep(def_wait)
    
# Класс, описывающий предмет на странице, который продается или покупается
# Основные переменные:
# my_buy_value - количестов предметов сколько я куплю - захардкожено.
# my_buy_price - цена по которой я куплю, высчитывается по формулам.
# my_sell_threshold - используется при продаже, если порог цены ниже моей стоимости продажи продажу надо отменить.
class SteamMarketItem:
    def __init__(self):
        self.error = 0
        self.is_unique = 1
        self.my_sell_threshold = 0
        self.best_sell_order_price = 0

        self.has_my_buy_order = 0
        self.has_my_sell_order = 0
        
        time.sleep(0.2)

        # Определяем уникальный предмет или нет.
        for i in range(8):
            try:
                find_item_by_id('searchResultsRows', 2)
                self.is_unique = 1
                break
            except:
                try:
                    find_item_by_id('market_commodity_forsale_table', 2)
                    self.is_unique = 0
                    break
                except:
                    driver.refresh()
                    time.sleep(0.5)
                    if i == 7:
                        self.error = 1
                        print("Failed to init SteamMarketItem while determining unique")
                        raise
                        return

        self.sell_orders_prices = []

        time.sleep(0.2)

        # Получаем лучшую цену покупки.
        if self.is_unique:
            for i in range(8):
                try:
                    buy_order_panel = find_item_by_id('market_buyorder_info')
                    time.sleep(0.1)
                    list = buy_order_panel.text.split('\n', 4)
                    time.sleep(0.1)

                    self.max_buy_order_price = get_float_from_str(list[2])
                    time.sleep(0.1)
                    break
                except:
                    driver.refresh()
                    time.sleep(0.5)
                    if i == 7:
                        self.error = 1
                        print("Failed to init SteamMarketItem while parsing buy orders")
                        raise
                        return

            actual_sale_table = 'searchResultsRows'
        else:
            for i in range(8):
                try:
                    buy_table = find_item_by_id('market_commodity_buyreqeusts_table')

                    time.sleep(0.1)
                    max_buy = buy_table.text.split('\n', 3)[1]
                    max_buy = max_buy.split(' ', 2)[0]
                    time.sleep(0.1)
                    self.max_buy_order_price = get_float_from_str(max_buy)
                    break
                except:
                    driver.refresh()
                    time.sleep(0.5)
                    if i == 7:
                        self.error = 1
                        print("Failed to init SteamMarketItem while parsing buy requests")
                        raise
                        return

            actual_sale_table = 'market_commodity_forsale_table'

        # Получаем спиксок цен продажи на первой странице.
        sale_table = find_item_by_id(actual_sale_table)
        time.sleep(0.2)
        lst = sale_table.text.split('\n')
        n = len(lst)

        for i in range(n):
            if lst[i][0].isdigit():
                sell_orders_strings = lst[i].split(' ', 2)[0]
                self.sell_orders_prices.append(get_float_from_str(sell_orders_strings))

                
        n = len(self.sell_orders_prices)
                    
        for i in range(n):         
            # Моя цена на продажу должна быть в пределах n лотов.
            if settings.resell_lot_position_val == 0:
                settings.resell_lot_position_val = 1
    
            if 0 <= i <= settings.resell_lot_position_val - 1:
                        self.my_sell_threshold = self.sell_orders_prices[i]

            if i == 0:
                self.best_sell_order_price = self.sell_orders_prices[i]
                        
                    
                    
        # Вычисляем мою цену покупки
        self.my_buy_price = self.sell_orders_prices[0] * settings.buy_percent_val

        if self.max_buy_order_price / self.my_buy_price < settings.buy_lower_percent_val:
            self.my_buy_price = self.max_buy_order_price / settings.buy_lower_percent_val

        self.my_buy_price = round(self.my_buy_price, 2)

        # Проверяем цену на правильность.
        if (self.my_buy_price > self.sell_orders_prices[0]):
            self.error = 1
            print("Failed to init SteamMarketItem: Buying price is invalid: " + str(self.my_buy_price))
            raise AssertionError('Buying price is invalid')
      
        self.my_buy_value = settings.buy_qty_val
        
        try:
            driver.find_element_by_id('my_market_buylistings_number')
            self.has_my_buy_order = 1
        except:
            pass

        try:
            driver.find_element_by_id('my_market_selllistings_number')
            self.has_my_sell_order = 1
        except:
            pass


# Класс, описывающий покупаемый предмет в списке предметов коллекции 1-10.
# Основные переменные:
# Qty - количество продающихся лотов.
# Price - цена наиболее дешевого лота.
class BuyItemInList:
    def __init__(self, ref):
        self.error = 0

        try:
            qty_fixed = ref.text.split('\n', 4)[0].replace(",", "")
            self.qty = get_int_from_str(qty_fixed)
            self.price = get_float_from_str(ref.text.split('\n', 4)[2])
        except:
            self.error = 1
            print("Failed to init BuyItemInList while parsing item")
            raise

# Функция входа на сайт Стима. Идентификатор телефона вводится вручную.
def login_to_steam(login, password):
    url = r'https://store.steampowered.com/login/'

    settings.msg_box['text'] = 'Not Connected'
    
    get_url(url, 1)
    
    # Вводим логин и пароль
    login_box = find_item_by_id('input_username')
    print_in_box(login_box, login)

    password_box = find_item_by_id('input_password')
    print_in_box(password_box, password)

    time.sleep(8)
    
    # Ждем пока сайт полностью не загрузится.
    while True:
       time.sleep(1)
       if driver.title.encode('utf8') == 'Добро пожаловать в Steam':
          settings.msg_box['text'] = 'Connected!'
          print("Connected to Steam account")
          break

       if settings.cancel_val:
          break

    time.sleep(0.1)
    settings.cancel_val = 0
    return


# Размещение заказа на покупку на странице вещи.
def place_order(price_val, quantity_val):

    # Ищем и жмем кнопку заказать
    order_btn = find_item_by_class_name('btn_green_white_innerfade')
    time.sleep(0.4)
    click_on_element(order_btn)

    if settings.cancel_val:
        return
        
    # Вводим цену за которую хотим купить
    price_box = find_item_by_id('market_buy_commodity_input_price')
    time.sleep(0.3)
    print_in_box(price_box, str(price_val))
    
    # Вводим количество, сколько купим
    qty_box = find_item_by_id('market_buy_commodity_input_quantity')
    time.sleep(0.3)
    print_in_box(qty_box, str(quantity_val))

    if settings.cancel_val:
        return
        
    # Проверяем правильная ли цена высветилась в окошке "Всего"
    total_box = find_item_by_id('market_buy_commodity_order_total')
    total_val = get_float_from_str(total_box.text)
    if total_val != price_val * quantity_val:
        print("Error in price! Real total: " + str(total_val) + " Calculated: " + str(price_val * quantity_val))
        return

    # Подтверждаем соглашение
    accept_checkbox = find_item_by_id('market_buyorder_dialog_accept_ssa')
    click_on_element(accept_checkbox)
    time.sleep(1)

    if settings.cancel_val:
        return
        
    # Покупаем
    buy_btn = find_item_by_id('market_buyorder_dialog_purchase')
    click_on_element(buy_btn)
    time.sleep(2.5)

    timeout = 100

    # Ждем пока сервер ответит нам - успех или ошибка при покупке
    while True:
        if settings.cancel_val:
            return

        if timeout == 0:
            print("Could not place buy order: Waiting timeout 20 sec expired")
            return
        timeout = timeout - 1
        time.sleep(0.2)

        # Успех - готово - все окей, выходим
        try:
            status = driver.find_element_by_id('market_buy_commodity_status')
            time.sleep(0.1)
            status_txt = status.text[:7]
            
            if (status_txt.encode('utf8') == 'Готово!'):
                global bought_items
                global bought_items_total_sum
                bought_items += 1
                bought_items_total_sum += total_val
                settings.msg_box['text'] = str(bought_items) + "  " + str(bought_items_total_sum)
                print("Buy order placed successfully. Bought: " + str(bought_items) + " Total sum: " + str(bought_items_total_sum))
                break
                
        except:
            pass

        # Ошибка - купить не смогли - выходим
        try:
            error = driver.find_element_by_id('market_buyorder_dialog_error_text')
            error_txt = error.text[:7]
            if error_txt.encode('utf8') == 'У вас у':
                print("Could not place buy order: Buy order already exists")
                break
            if error_txt.encode('utf8') == 'Извинит':
                print("Could not place buy order: Server error")
                break
            if error_txt.encode('utf8') == 'Этот за':
                print("Could not place buy order: Out of money")
                settings.cancel_val = 1
                break  
        except:
            continue
    return

# Просмотр и покупка вещей на странице просмотра коллекции, если они удовлетворяют условиям.
def find_items_on_page(url_page):
    
    get_url(url_page)
    time.sleep(1)
    
    # Проходим по всем 10 элементам на странице.
    for i in range(0, 9):
        if settings.cancel_val:
            return
            
        # Создаем класс вещи для покупки.
        try:
            found_item_in_list = find_item_by_id('result_' + str(i))
            time.sleep(0.1)
            buy_item = BuyItemInList(found_item_in_list)
        except:
            print("Could not find item element " + str(i))
            continue
        
        if settings.cancel_val:
            return
            
        # Если удовлетворяет условиям - пытаемся купить.
        if buy_item.qty >= settings.min_qty_val:
            if settings.min_price_val <= buy_item.price <= settings.max_price_val :
                time.sleep(0.3)

                try:
                    open_link_in_new_window(found_item_in_list)
                except:
                    print("Could not click on element " + str(i) + " while trying to buy ")
                    continue

                try:
                    item = SteamMarketItem()

                    if not item.has_my_buy_order:
                        place_order(item.my_buy_price, item.my_buy_value)
                        time.sleep(0.3)
                    else:
                        print("My buy order already exist for this item")
                        time.sleep(0.1)

                except:
                    print("Could not place buy order " + str(i))

                close_link()
        

# Получение количества страниц в коллекции.
def get_pages():
    try:
        pages_txt = find_item_by_id("searchResults_links")
        last_page = pages_txt.text.rsplit(' ', 1)[1]
        return int(last_page)
    except:
        print("Could not get pages in collection. Returning 1")
        return 1


# Покупка вещей из коллекции.
def buy_items_in_collection(cur_collection_url):

    driver.get(cur_collection_url)
    
    pages = get_pages()
    
    if (settings.start_page_val > pages+1):
      print("Start page is higher than last collection page")
      return
    
    # Проходим по всем страницам в коллекции и смотрим что там есть интересного.
    for i in range(settings.start_page_val, pages+1):
        page_str = page_ascending_sort.replace("ID", str(i))

        find_items_on_page(cur_collection_url + page_str)

        if settings.cancel_val:
            return


# Покупка вещей из коллекции CSGO.
def buy_items_csgo(collection_tag):
    cur_collection_url = collection_csgo_url.replace("INSERTTAGHERE", collection_tag)
    buy_items_in_collection(cur_collection_url)

# Покупка всех вещей из Артифакта.
def buy_items_artifact(collection_tag):
    buy_items_in_collection(collection_artifact_url)

# Покупка всех вещей из Доты.
def buy_items_dota(collection_tag):
    buy_items_in_collection(collection_dota_url)

# Покупка всех вещей из Тим фортреса.
def buy_items_tf(collection_tag):
    buy_items_in_collection(collection_tf_url)

# Функция покупки вещей.
# Функция вызывается по кнопке.
def place_buy_orders():
    global bought_items
    global bought_items_total_sum

    bought_items = 0
    bought_items_total_sum = 0

    settings.msg_box['text'] = str(bought_items)

    # Покупаем вещи из игр где есть галочки.
    n = len(settings.tags_csgo)
    for i in range(n):
        if settings.tags_csgo[i][2].get():
            buy_items_csgo(settings.tags_csgo[i][0])

        if settings.cancel_val:
            break

    n = len(settings.tags_artifact)
    for i in range(n):
        if settings.tags_artifact[i][2].get():
            buy_items_artifact(settings.tags_artifact[i][0])

        if settings.cancel_val:
            break

    n = len(settings.tags_dota)
    for i in range(n):
        if settings.tags_dota[i][2].get():
            buy_items_dota(settings.tags_dota[i][0])

        if settings.cancel_val:
            break
            
    n = len(settings.tags_tf)
    for i in range(n):
        if settings.tags_tf[i][2].get():
            buy_items_tf(settings.tags_tf[i][0])

        if settings.cancel_val:
            break

    settings.msg_box['text'] = 'Buying done'
    settings.cancel_val = 0
    driver.get(main_url)
    return


# Функция покупки вещей с текущей страницы и до конца страниц.
# Сделана для возможности покупки любых вещей.
# Функция вызывается по кнопке.
def buy_fcp():
    cur_url = driver.current_url
    cur_page = 1

    last_mod = page_popular_sort

    try:
        last_mod = curUrl.rsplit('#', 1)[1]

        if last_mod[0] == 'p':
            cur_page = get_int_from_str(last_mod)
            cur_url = cur_url.rsplit('#', 1)[0]

            last_mod = re.sub(r'[0-9]+', '', last_mod)

            last_mod = last_mod[1:]

            last_mod = "#pID" + last_mod
    except:
        print("failed to get page" + last_mod)
        return

    pages = get_pages()

    for i in range(cur_page, pages + 1):
        page_str = lastMod.replace("ID", str(i))

        find_items_on_page(cur_url + page_str)

        if settings.cancel_val:
            break

    settings.msg_box['text'] = 'Buying fcp done'
    settings.cancel_val = 0
    driver.get(main_url)
    return


# Отмена ордеров на покупку, если цена ушла слишком низко или высоко.
# Функция вызывается по кнопке.
def check_buy_orders_prices():
    get_url(market_url)
    time.sleep(2)

    try:
        items_to_buy_txt = find_item_by_id("my_market_buylistings_number",3)
        items_to_buy_num = get_int_from_str(items_to_buy_txt.text)
        items_to_buy = items_to_buy_num
    except:
        print("no any buy lisitng found")
        return

    i = settings.check_start_from_val - 1

    if settings.check_end_from_val:
        items_to_buy = settings.check_end_from_val - i

    if items_to_buy > items_to_buy_num:
        items_to_buy = items_to_buy_num
        


    print("Checking buy orders from " + str(i) + " to " + str(items_to_buy))
    
    # Проверяем все заказы
    while i < items_to_buy:
        progress_str = str(i+1) + "/" + str(items_to_buy)
        settings.msg_box['text'] = progress_str
        print("Checking buy order: " + progress_str)
        
        # Ищем все заказы на покупку
        try:
            buy_orders_cancels = driver.find_elements_by_xpath(
                "//a[starts-with(@href, 'javascript:CancelMarketBuyOrder')]")
            buy_orders_names = driver.find_elements_by_xpath(
                "//a[starts-with(@href, 'javascript:CancelMarketBuyOrder')]/ancestor::div[3]//div[@class='market_listing_item_name_block']//span[starts-with(@class, 'market_listing_item_name')]//a[@class='market_listing_item_name_link']")
            buy_orders_prices = driver.find_elements_by_xpath(
                "//a[starts-with(@href, 'javascript:CancelMarketBuyOrder')]/ancestor::div[3]")

        except:
            print("Could not find buy orders table. Probably you dont have any buy orders")
            break
        
        if settings.cancel_val:
            break

        try:
            my_buy_price = get_float_from_str(buy_orders_prices[i].text.split('\n', 2)[0])
        except:
            print("Could not get price from buy order. Probably buy order is disappeared")
            i += 1
            continue

        time.sleep(0.1)

        try:
          open_link_in_new_window(buy_orders_names[i])
        except:
          print("Could not click on buy order item. Probably buy order is disappeared")
          i += 1
          continue
        
        try:
            item = SteamMarketItem()
        except:
            i += 1
            continue
        finally:
            close_link()

        if settings.cancel_val:
            break

        if (my_buy_price >= item.best_sell_order_price * settings.check_max_percent_val) or (my_buy_price <= item.best_sell_order_price * settings.check_min_percent_val):

            try:
                items_before_txt = find_item_by_id("my_market_buylistings_number")
                items_before = get_int_from_str(items_before_txt.text)

                click_on_element(buy_orders_cancels[i])

                timeout = 30
                while True:
                    if timeout == 0:
                        print("Cancel buy order " + str(k + 1) + " failed: order timeout")
                        break

                    timeout = timeout - 1
                    time.sleep(0.5)

                    items_after_txt = find_item_by_id("my_market_buylistings_number")
                    items_after = get_int_from_str(items_after_txt.text)

                    if items_after == items_before - 1:
                        break

            except:
                print("Check failed. Checking buy order " + str(i+1) + " failed. Probably buy order is disappeared")

            print("Checked successfully. Cancelled buy order " + str(i+1))
            items_to_buy -= 1
        else:
            print("Checked successfully. No need to cancel buy order " + str(i+1))
            i += 1

        

    settings.msg_box['text'] = 'Checking prices done'
    settings.cancel_val = 0
    driver.get(main_url)
    return


# Отмена всех ордеров на покупку.
# Функция вызывается по кнопке.
def cancel_all_buy_orders():
    get_url(market_url)
    time.sleep(2)

    try:
        items_to_buy_txt = find_item_by_id("my_market_buylistings_number",3)
        items_to_buy = get_int_from_str(items_to_buy_txt.text)
    except:
        print("no any buy lisitng found")
        return

    i = settings.check_start_from_val - 1

    print("Cancelling buy orders from " + str(i) + " to " + str(items_to_buy))

    while i < items_to_buy:
        progress_str = str(i+1) + "/" + str(items_to_buy)
        settings.msg_box['text'] = progress_str
        print("Deleting buy order: " + progress_str)
        
        try:
            buy_orders_cancels = driver.find_elements_by_xpath(
                "//a[starts-with(@href, 'javascript:CancelMarketBuyOrder')]")
        except:
            print("Could not find buy orders table. Probably you dont have any buy orders")
            break
        
        if settings.cancel_val:
            break

        try:
            items_before_txt = find_item_by_id("my_market_buylistings_number")
            items_before = get_int_from_str(items_before_txt.text)

            click_on_element(buy_orders_cancels[i])

            timeout = 30
            while True:
                if timeout == 0:
                    print("Cancel buy order " + str(k + 1) + " failed: order timeout")
                    break

                timeout = timeout - 1
                time.sleep(0.5)

                items_after_txt = find_item_by_id("my_market_buylistings_number")
                items_after = get_int_from_str(items_after_txt.text)

                if items_after == items_before - 1:
                    break

            print("Deleted successfully. Cancelled buy order " + str(i+1))
        except:
            print("Delete failed. Cancelling order " + str(i) + " failed. Probably buy order is disappeared")

        items_to_buy -= 1

    settings.msg_box['text'] = 'Cancel all done'
    settings.cancel_val = 0
    driver.get(main_url)
    return


# Подсчет всех ордеров на покупку - их цен.
# Функция вызывается по кнопке.
def calc_buy_orders():
    get_url(market_url)
    time.sleep(2)

    buy_orders_sum = 0
    
    try:
        items_to_buy_txt = find_item_by_id("my_market_buylistings_number",3)
        items_to_buy = get_int_from_str(items_to_buy_txt.text)
    except:
        print("no any buy lisitng found")
        return

    print("Calculating buy orders: " + str(items_to_buy))

    settings.msg_box['text'] = '...'

    try:
        buy_orders_prices = driver.find_elements_by_xpath("//a[starts-with(@href, 'javascript:CancelMarketBuyOrder')]/ancestor::div[3]")
    except:
        print("Could not find buy orders table. Probably you dont have any buy orders")
        return
        
    n = len(buy_orders_prices)
        
    for i in range(n):
                
        if settings.cancel_val:
            break

        try:

            my_buy_price = get_float_from_str(buy_orders_prices[i].text.split('\n', 2)[0])
            my_buy_cnt = get_float_from_str(buy_orders_prices[i].text.split('\n', 2)[1])

            buy_orders_sum += my_buy_price*my_buy_cnt
        except:
            print("Could not get price from buy order. Probably buy order is disappeared")

    balance_txt = find_item_by_id("marketWalletBalance", 3)

    balance_val = get_float_from_str(balance_txt.text)

    balance_val = balance_val * 10


    settings.msg_box['text'] = 'Buy orders sum: ' + str(buy_orders_sum) + '. You can place orders on: ' + str(balance_val-buy_orders_sum) + ' RUB'
    settings.cancel_val = 0
    driver.get(main_url)
    return
    
    
# Расчет суммы продажи.
def calc_sell_price(prices):
    n = len(prices)

    percents = list()

    for i in range(n-1):
        sub = prices[i+1] - prices[i]
        percents.append(sub/prices[i])

    y = len(percents)

    for i in range(y):
        if percents[i] > settings.gap_percent_val:
            return prices[i+1] - settings.place_lower_val

    return prices[0] - settings.place_lower_val


# Функция продажи вещей из какой либо игры
def sell_items_from_game(sell_game_id):
    my_sell_url = sell_url.replace("INSERTIDHERE", settings.link_id_val)
    my_sell_url = my_sell_url + sell_game_id
    time.sleep(0.5)
    
    get_url(my_sell_url)
    time.sleep(2.4)
    
    
    SCROLL_PAUSE_TIME = 0.8

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    while True:
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height
   
    time.sleep(2.4)
    
    
    try:
      find_item_by_class_name("inventory_item_link")
      sell_items_list = driver.find_elements(By.CLASS_NAME, 'inventory_item_link')
      n = len(sell_items_list)
    except:
      n = 0
      print("Could not find any items to sell. Probably you dont have one")
    
    print("Selling " + str(n) + " items from inventory game " + sell_game_id)
    
    # Продаем все найденные вещи
    for k in range(n):
        progress_str = str(k+1) + "/" + str(n)
        settings.msg_box['text'] = progress_str
        print("Selling item: " + progress_str)
        
        time.sleep(0.4)
        
        try:
            click_on_element(sell_items_list[k])
        except:
            print("Could not click on item while selling. Probably sell item is disappeared")
            continue
      
        if settings.cancel_val:
            break

        try:
            find_item_by_xpath("//div[@style='height: 24px;']")
            ref = driver.find_elements_by_xpath("//div[@style='height: 24px;']")

            open_link_in_new_window(ref[1])
        except:
            print("Could not find `find on market` string. Probably sell item is disappeared")
            continue
        
        try:
            item = SteamMarketItem()
        except:
            continue
        finally:
            close_link()
    
        if settings.cancel_val:
            break

        try:
            click_on_button_scr("//a[@href='javascript:SellCurrentSelection()']")
        except:
            print("Could not click on sell button.")
            continue

        if settings.cancel_val:
            break

        price_val = calc_sell_price(item.sell_orders_prices)

        sell_box = find_item_by_id('market_sell_buyercurrency_input')
        print_in_box(sell_box, str(price_val))

        accept_checkbox = find_item_by_id('market_sell_dialog_accept_ssa')
        if not accept_checkbox.is_selected():
            click_on_element(accept_checkbox)

        if settings.cancel_val:
            break
            

        click_on_button_scr("//a[@id='market_sell_dialog_accept']")

        # Жмем на кнопку Ок при продаже
        click_on_button_scr("//a[@id='market_sell_dialog_ok']")
   
        timeout = 20

        while True:
            if timeout == 0:
                print("Sell item " + str(k+1) + " failed: order timeout")
                break
                
            timeout = timeout - 1
            time.sleep(0.4)

            try:  
                find_item_by_xpath("//div[starts-with(@class, 'btn_grey_white_innerfade')]/ancestor::div[@class='newmodal_buttons']", 0.1)
                print("Sell item " + str(k+1) + " succeeded")
                break
            except:
                pass
                
            error = driver.find_element_by_id('market_sell_dialog_error')
                
            if error.text.encode('utf8') != '':
                error_txt = error.text[:7]
                
                if error_txt.encode('utf8') == 'Лот с э':
                    print("Sell item " + str(k+1) + " failed: order already exists")
                else:
                    print("Sell item " + str(k+1) + " failed: server error")
                    
                break
    
        click_on_button_perform("//div[@class='newmodal_close']")

        try:
            btn = driver.find_element_by_xpath("//div[@class='economy_item_popup_dismiss']")
            driver.execute_script("(arguments[0]).click();", btn)
            time.sleep(0.1)          
        except:
            pass
       
        if settings.cancel_val:
            break

        

    print("Selling items from game " + sell_game_id + " done")
    return


# Функция продажи вещей из инвентаря
# Функция вызывается по кнопке.
def sell_items_from_inventory():

    n = len(settings.sell_games_ids)
    for i in range(n):
        if settings.sell_games_ids[i][2].get():
            sell_items_from_game(settings.sell_games_ids[i][0])

        if settings.cancel_val:
            break

    settings.msg_box['text'] = 'Sell done'
    settings.cancel_val = 0
    driver.get(main_url)
    return

# Отмена продажи всех вещей.
# Функция вызывается по кнопке.
def cancel_all_sell_orders():
    get_url(market_url)
    time.sleep(2)

    try:
        items_to_sell_txt = find_item_by_id("my_market_selllistings_number", 3)
        items_to_sell = get_int_from_str(items_to_sell_txt.text)
    except:
        print("no any sell listing found")
        return

    i = settings.sell_start_from_val - 1

    if settings.sell_end_from_val:
        items_to_sell = settings.sell_end_from_val - i

    try:
        page10_btn = find_item_by_id('my_listing_pagesize_10')
        click_on_element(page10_btn)
        time.sleep(2)
        items_per_page = 10
    except:
        sell_orders_cancels = driver.find_elements_by_xpath("//a[starts-with(@href, 'javascript:RemoveMarketListing')]")
        items_per_page = len(sell_orders_cancels)
        print("no 'results 10' element found. Setting items per page: " + str(items_per_page))

    try:
        pages_num = driver.find_elements_by_class_name("market_paging_pagelink")
        n = len(pages_num)
        last_page_num = get_int_from_str(pages_num[n - 1].text)
        click_on_element(pages_num[n - 1])
        time.sleep(5)
    except:
        print("no pages element found. Setting pages: 1")
        last_page_num = 1

    print("Cancelling sell orders from " + str(i) + " to " + str(items_to_sell))

    for j in range(last_page_num):
        cur_item = 0

        for i in range(items_per_page):

            progress_str = str(j * (last_page_num - 1) + i + 1) + "/" + str(items_to_sell)
            settings.msg_box['text'] = progress_str
            print("Cancelling sell order " + progress_str)

            if settings.cancel_val:
                break

            try:
                sell_orders_cancels = driver.find_elements_by_xpath(
                    "//a[starts-with(@href, 'javascript:RemoveMarketListing')]")
            except:
                print("Could not find sell orders table. Probably you dont have any sell orders")
                break

            time.sleep(1)


            if settings.cancel_val:
                break

            try:
                click_on_element(sell_orders_cancels[cur_item])
                time.sleep(0.2)
                cancel = find_item_by_id('market_removelisting_dialog_accept')
                click_on_element(cancel)
                time.sleep(0.5)

                timeout = 20
                while True:

                    if timeout == 0:
                        print("Error sell window timeout")
                        break

                    time.sleep(0.5)
                    timeout -= 1

                    try:
                        cancel_wnd = driver.find_element_by_id('market_removelisting_dialog')

                        is_active = "display: block" in cancel_wnd.get_attribute("style")

                        if not is_active:
                            break

                    except:
                        print("Market close window not found")
                        break

                time.sleep(0.5)
                print("Resell succeeded. Cancelled sell order " + str(i))
                continue

            except:
                print("could not resell sell order " + str(i))

            cur_item += 1

        if j == last_page_num - 1:
            break

        if settings.cancel_val:
            break

        try:
            prev_btn = find_item_by_id('tabContentsMyActiveMarketListings_btn_prev')
            time.sleep(0.2)
            click_on_element(prev_btn)
            time.sleep(1)

            timeout = 30
            while True:

                if timeout == 0:
                    print("Error prev button timeout")
                    break

                time.sleep(0.5)
                timeout -= 1

                try:
                    elem = driver.find_element_by_xpath("//span[starts-with(@class, 'market_paging_pagelink active')]")
                    cur_page = get_int_from_str(elem.text)

                    if cur_page == last_page_num - j - 1:
                        break

                except:
                    print("Not found pagelink")
                    continue

        except:
            print("Could not find prev button")
            continue

    settings.msg_box['text'] = 'Cancel all sell done'
    settings.cancel_val = 0
    driver.get(main_url)
    return
    
    
# Перепродажа вещей - если некотрые вещи продаются слишком высоко - они снимаются и продаются заново.
# Функция вызывается по кнопке.
def resell_items():   
    get_url(market_url)
    time.sleep(2)

    try:
        items_to_sell_txt = find_item_by_id("my_market_selllistings_number", 3)
        items_to_sell = get_int_from_str(items_to_sell_txt.text)
    except:
        print("no any sell listing found")
        return

    i = settings.sell_start_from_val - 1
    
    if settings.sell_end_from_val:
        items_to_sell = settings.sell_end_from_val - i

    try:
        page10_btn = find_item_by_id('my_listing_pagesize_10')
        click_on_element(page10_btn)
        time.sleep(2)
        items_per_page = 10
    except:
        sell_orders_names = driver.find_elements_by_xpath("//span[starts-with(@id, 'mylisting_')]//a[@class='market_listing_item_name_link']")
        items_per_page = len(sell_orders_names)
        print("no 'results 10' element found. Setting items per page: " + str(items_per_page))

    try:
        pages_num = driver.find_elements_by_class_name("market_paging_pagelink")
        n = len(pages_num)
        last_page_num = get_int_from_str(pages_num[n-1].text)
        click_on_element(pages_num[n - 1])
        time.sleep(5)
    except:
        print("no pages element found. Setting pages: 1")
        last_page_num = 1

    print("Reselling sell orders from " + str(i) + " to " + str(items_to_sell))

    for j in range(last_page_num):
        cur_item = 0

        for i in range(items_per_page):

            progress_str = str(j*(last_page_num-1) + i + 1) + "/" + str(items_to_sell)
            settings.msg_box['text'] = progress_str
            print("Checking sell order " + progress_str)

            if settings.cancel_val:
                break

            try:
                sell_orders_cancels = driver.find_elements_by_xpath("//a[starts-with(@href, 'javascript:RemoveMarketListing')]")
                sell_orders_names =   driver.find_elements_by_xpath("//a[starts-with(@href, 'javascript:RemoveMarketListing')]/ancestor::div[3]//div[@class='market_listing_item_name_block']//span[starts-with(@class, 'market_listing_item_name economy_item_hoverable')]//a[@class='market_listing_item_name_link']")
                sell_orders_prices =  driver.find_elements_by_xpath("//a[starts-with(@href, 'javascript:RemoveMarketListing')]/ancestor::div[3]")

            except:
                print("Could not find sell orders table. Probably you dont have any sell orders")
                break

            time.sleep(1)

            try:
                my_sell_price = get_float_from_str(sell_orders_prices[cur_item].text.split('\n', 2)[0])
            except:
                print("Could not get price from sell order. Probably sell order is disappeared")
                i += 1
                cur_item += 1
                continue

            try:
                href = sell_orders_names[cur_item].get_attribute("href")
                open_link_in_new_window_by_url(href)

            except:
                print("Could not click on sell order item. Probably sell order is disappeared")
                i += 1
                cur_item += 1
                raise

            try:
                item = SteamMarketItem()
            except:
                i += 1
                cur_item += 1
                continue
            finally:
                close_link()

            time.sleep(0.4)

            actions = ActionChains(driver)
            items_to_sell_txt = find_item_by_id("my_market_selllistings_number")
            actions.move_to_element(items_to_sell_txt).click().perform()
            time.sleep(0.4)

            if settings.cancel_val:
                break

            try:
                if my_sell_price >= item.my_sell_threshold:
                    click_on_element(sell_orders_cancels[cur_item])
                    time.sleep(0.2)
                    cancel = find_item_by_id('market_removelisting_dialog_accept')
                    click_on_element(cancel)
                    time.sleep(0.5)

                    timeout = 20
                    while True:

                        if timeout == 0:
                            print("Error sell window timeout")
                            break

                        time.sleep(0.5)
                        timeout -= 1

                        try:
                            cancel_wnd = driver.find_element_by_id('market_removelisting_dialog')

                            is_active = "display: block" in cancel_wnd.get_attribute("style")

                            if not is_active:
                                break

                        except:
                            print("Market close window not found")
                            break

                    time.sleep(0.5)
                    print("Resell succeeded. Cancelled sell order " + str(i))
                    continue
                else:
                    print("Resell succeeded. No need to cancel sell order " + str(i))
            except:
                print("could not resell sell order " + str(i))

            cur_item += 1

        if j == last_page_num - 1:
            break

        if settings.cancel_val:
            break

        try:
            prev_btn = find_item_by_id('tabContentsMyActiveMarketListings_btn_prev')
            time.sleep(0.2)
            click_on_element(prev_btn)
            time.sleep(1)

            timeout = 30
            while True:

                if timeout == 0:
                    print("Error prev button timeout")
                    break

                time.sleep(0.5)
                timeout -= 1

                try:
                    elem = driver.find_element_by_xpath("//span[starts-with(@class, 'market_paging_pagelink active')]")
                    cur_page = get_int_from_str(elem.text)

                    if cur_page == last_page_num - j - 1:
                        break

                except:
                    print("Not found pagelink")
                    continue

        except:
            print("Could not find prev button")
            continue

    if not settings.cancel_val:
        print("Now selling items...")
        sell_items_from_inventory()

    settings.msg_box['text'] = 'Resell done'
    settings.cancel_val = 0
    driver.get(main_url)
    return

def cancel_confirming():
    get_url(market_url)

    time.sleep(2)

    try:
        items_to_cancel_txt = find_item_by_id("my_market_listingstoconfirm_number", 3)
        items_to_cancel = get_int_from_str(items_to_cancel_txt.text)
    except:
        print("no any confirming orders found")
        return

    i = 0

    print("Cancelling buy confirm orders from " + str(i) + " to " + str(items_to_cancel))

    while i < items_to_cancel:
        progress_str = str(i + 1) + "/" + str(items_to_cancel)
        settings.msg_box['text'] = progress_str
        print("Deleting confirm order: " + progress_str)

        try:
            orders_cancels = driver.find_elements_by_xpath(
                "//a[starts-with(@href, 'javascript:CancelMarketListingConfirmation')]")
        except:
            print("Could not find buy orders table. Probably you dont have any buy orders")
            break

        if settings.cancel_val:
            break

        try:
            items_before_txt = find_item_by_id("my_market_listingstoconfirm_number")
            items_before = get_int_from_str(items_before_txt.text)

            click_on_element(orders_cancels[i])
            time.sleep(0.2)
            cancel = find_item_by_id('market_removelisting_dialog_accept')
            click_on_element(cancel)
            time.sleep(0.5)

            timeout = 20
            while True:

                if timeout == 0:
                    print("Error sell window timeout")
                    break

                time.sleep(0.5)
                timeout -= 1

                try:
                    cancel_wnd = driver.find_element_by_id('market_removelisting_dialog')

                    is_active = "display: block" in cancel_wnd.get_attribute("style")

                    if not is_active:
                        break

                except:
                    print("Market close window not found")
                    break

            time.sleep(0.5)


            timeout = 30
            while True:
                if timeout == 0:
                    print("Cancel confirm order " + str(k + 1) + " failed: order timeout")
                    break

                timeout = timeout - 1
                time.sleep(0.5)

                items_after_txt = find_item_by_id("my_market_listingstoconfirm_number")
                items_after = get_int_from_str(items_after_txt.text)

                if items_after == items_before - 1:
                    break

            print("Deleted successfully. Cancelled confirm order " + str(i + 1))
        except:
            print("Delete failed. Cancelling confirm order " + str(i) + " failed. Probably order is disappeared")

        items_to_cancel -= 1

    settings.msg_box['text'] = 'Cancel confirm all done'
    settings.cancel_val = 0
    driver.get(main_url)
    return




# Закрытие драйвера.
def close_dr():
    driver.close()
