# -*- coding: utf-8 -*-
# Настройки программы по умолчанию.

from Tkinter import *
from ttk import *

def init():
    #global
    global cancel_val
    cancel_val = 0

    global login_val
    login_val = "bohmannn"
    global passwd_val
    passwd_val = "-"
    global link_id_val
    link_id_val = "profiles/76561198028876338"

    #buy
    global buy_percent_val
    buy_percent_val = 0.73
    global buy_lower_percent_val
    buy_lower_percent_val = 1.08
    global buy_qty_val
    buy_qty_val = 1
    global min_price_val
    min_price_val = 50
    global max_price_val
    max_price_val = 1000
    global min_qty_val
    min_qty_val = 40
    global start_page_val
    start_page_val = 1
    
    #check
    global check_min_percent_val
    check_min_percent_val = 0.55
    global check_max_percent_val
    check_max_percent_val = 0.84
    global check_start_from_val
    check_start_from_val = 1
    global check_end_from_val
    check_end_from_val = 0
    
    #sell
    global gap_percent_val
    gap_percent_val = 0.04
    global place_lower_val
    place_lower_val = 0.10
    global sell_start_from_val
    sell_start_from_val = 1
    global sell_end_from_val
    sell_end_from_val = 0
    global resell_lot_position_val
    resell_lot_position_val = 5


    global msg_box
    msg_box = 0
    
    global tags_csgo
    tags_csgo = list()
    tags_csgo.append(["tag_set_inferno_2","2018 Inferno",0])
    tags_csgo.append(["tag_set_nuke_2","2018 Nuke",0])
    tags_csgo.append(["tag_set_community_20","Horizon",0])
    tags_csgo.append(["tag_set_community_19","Clutch",0])
    tags_csgo.append(["tag_set_community_18","Spectrum2",0])
    tags_csgo.append(["tag_set_community_17","Op Hydra",0])
    tags_csgo.append(["tag_set_community_16","Spectrum",0])
    tags_csgo.append(["tag_set_community_15","Glove",0])
    tags_csgo.append(["tag_set_gamma_2","Gamma2",0])
    tags_csgo.append(["tag_set_community_13","Gamma",0])
    tags_csgo.append(["tag_set_community_12","Chroma3",0])
    tags_csgo.append(["tag_set_community_11","Wildfire",0])
    tags_csgo.append(["tag_set_community_10","Revolver",0])
    tags_csgo.append(["tag_set_community_9","Shadow",0])
    tags_csgo.append(["tag_set_community_8","Falchion",0])
    tags_csgo.append(["tag_set_gods_and_monsters","G and M",0])
    tags_csgo.append(["tag_set_chopshop","Chop Shop",0])
    tags_csgo.append(["tag_set_kimono","Rising Sun",0])
    tags_csgo.append(["tag_set_community_7","Chroma2",0])
    tags_csgo.append(["tag_set_community_6","Chroma",0])
    tags_csgo.append(["tag_set_community_5", "Vanguard", 0])
    tags_csgo.append(["tag_set_cache", "Cache", 0])
    tags_csgo.append(["tag_set_overpass", "Overpass", 0])
    tags_csgo.append(["tag_set_cobblestone", "CobbleSt", 0])
    tags_csgo.append(["tag_set_baggage", "Baggage", 0])
    tags_csgo.append(["tag_set_community_4", "Breakout", 0])
    tags_csgo.append(["tag_set_community_3", "Huntsman", 0])
    tags_csgo.append(["tag_set_bank", "Bank", 0])
    tags_csgo.append(["tag_set_community_2", "Phoenix", 0])
    tags_csgo.append(["tag_set_esports_ii", "esp2013W", 0])
    tags_csgo.append(["tag_set_weapons_iii", "Arms Deal3", 0])
    tags_csgo.append(["tag_set_community_1", "Wint Off", 0])
    tags_csgo.append(["tag_set_train", "Train", 0])
    tags_csgo.append(["tag_set_dust_2", "Dust2", 0])
    tags_csgo.append(["tag_set_mirage", "Mirage", 0])
    tags_csgo.append(["tag_set_lake", "Lake", 0])
    tags_csgo.append(["tag_set_italy", "Italy", 0])
    tags_csgo.append(["tag_set_safehouse", "Safehouse", 0])
    tags_csgo.append(["tag_set_bravo_i", "Bravo", 0])
    tags_csgo.append(["tag_set_weapons_ii", "Arms Deal2", 0])
    tags_csgo.append(["tag_set_bravo_ii", "Alpha", 0])
    tags_csgo.append(["tag_set_aztec", "Aztec", 0])
    tags_csgo.append(["tag_set_dust", "Dust", 0])
    tags_csgo.append(["tag_set_vertigo", "Vertigo", 0])
    tags_csgo.append(["tag_set_militia", "Militia", 0])
    tags_csgo.append(["tag_set_inferno", "Inferno", 0])
    tags_csgo.append(["tag_set_nuke", "Nuke", 0])
    tags_csgo.append(["tag_set_office", "Office", 0])
    tags_csgo.append(["tag_set_esports", "eSports", 0])
    tags_csgo.append(["tag_set_assault", "Assault", 0])
    tags_csgo.append(["tag_set_weapons_i", "Arms Deal", 0])

    global tags_artifact
    tags_artifact = list()
    tags_artifact.append(["", "All items", 0])
    
    global tags_dota
    tags_dota = list()
    tags_dota.append(["", "All items", 0])
    
    global tags_tf
    tags_tf = list()
    tags_tf.append(["", "All items", 0])

    
    global sell_games_ids
    sell_games_ids = list()
    sell_games_ids.append(["730", "CSGO", 1])
    sell_games_ids.append(["583950", "Artifact", 0])
    sell_games_ids.append(["570", "Dota", 0])
    sell_games_ids.append(["440", "Team Fort", 0])
    