# -*- coding: utf-8 -*-
# Главное окно программы

from Tkinter import *
from ttk import *
import threading

import settings
import logic

settings.init()

# Кнопка логин.
def login_to():
    settings.login_val = login_box.get()
    settings.passwd_val = passwd_box.get()
    settings.link_id_Val = link_id_box.get()
    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.login_to_steam, args=(settings.login_val, settings.passwd_val))
    thd.start()


# Кнопка купить.
def buy():
    settings.cancel_val = 0
    settings.buy_percent_val = float(buy_percent_box.get())
    settings.buy_lower_percent_val = float(buy_lower_percent_box.get())
    settings.buy_qty_val = int(buy_qty_box.get())
    settings.min_price_val = int(min_price_box.get())
    settings.max_price_val = int(max_price_box.get())
    settings.min_qty_val = int(min_qty_box.get())
    settings.start_page_val = int(start_page_box.get())
    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.place_buy_orders)
    thd.start()

# Кнопка купить с текущей страницы.
def buy_fcp():
    settings.cancel_val = 0
    settings.buy_percent_val = float(buy_percent_box.get())
    settings.buy_lower_percent_val = float(buy_lower_percent_box.get())
    settings.buy_qty_val = int(buy_qty_box.get())
    settings.min_price_val = int(min_price_box.get())
    settings.max_price_val = int(max_price_box.get())
    settings.min_qty_val = int(min_qty_box.get())
    settings.start_page_val = int(start_page_box.get())
    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.buy_fcp)
    thd.start()

# Кнопка проверить ордера на покупку.
def check():
    settings.cancel_val = 0
    settings.check_min_percent_val = float(check_min_percent_box.get())
    settings.check_max_percent_val = float(check_percent_box.get())
    settings.check_start_from_val = int(check_start_from_box.get())
    settings.check_end_from_val = int(check_end_from_box.get())
    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.check_buy_orders_prices)
    thd.start()


# Кнопка отменить все ордера на покупку.
def cancel_all():
    settings.cancel_val = 0
    settings.check_start_from_val = int(check_start_from_box.get())
    settings.check_end_from_val = int(check_end_from_box.get())

    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.cancel_all_buy_orders)
    thd.start()

def calc_buy_orders():
    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.calc_buy_orders)
    thd.start()


# Кнопка продать вещи из инвентаря.
def sell():
    settings.cancel_val = 0
    settings.gap_percent_val = float(gap_percent_box.get())
    settings.place_lower_val = float(place_lower_box.get())
    settings.sell_start_from_val = int(sell_start_from_box.get())
    settings.sell_end_from_val = int(sell_end_from_box.get())


    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.sell_items_from_inventory)
    thd.start()

# Кнопка отменить продажу всех вещей.
def sell_cancel():
    settings.cancel_val = 0

    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.cancel_all_sell_orders)
    thd.start()
    
# Кнопка перепродать вещи из инвентаря.
def resell():
    settings.cancel_val = 0
    settings.gap_percent_val = float(gap_percent_box.get())
    settings.sell_start_from_val = int(sell_start_from_box.get())
    settings.sell_end_from_val = int(sell_end_from_box.get())

    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.resell_items)
    thd.start()

# Кнопка отменить продажу ожидающих предметов
def cancel_confirming():
    settings.cancel_val = 0

    settings.msg_box[ 'text' ] = '                '

    thd = threading.Thread(target=logic.cancel_confirming)
    thd.start()

# Кнопка отменить последнее действие.
def cancel_btn():
    settings.cancel_val = 1
    settings.msg_box[ 'text' ] = 'Cancelling...'


# Инициализация главного окна.
root = Tk()
root.title('Buy&Sell Trading Helper v1.36')
root.geometry('480x790')

Label( root, text='Do NOT change Chrome Browser window dimensions!' ).pack( side=TOP )

Label( root, text='Login: ' ).pack( side=TOP )
login_box = StringVar()
Entry( root, width=30, textvariable = login_box).pack( side= TOP )
login_box.set( settings.login_val )

Label( root, text='Password: ' ).pack( side=TOP )
passwd_box = StringVar()
Entry( root, width=30, textvariable = passwd_box).pack( side= TOP )
passwd_box.set( settings.passwd_val )

Label( root, text='Sell link Id: https://steamcommunity.com/!LINK!/inventory?modal=1' ).pack( side=TOP )
link_id_box = StringVar()
Entry( root, width=40, textvariable = link_id_box).pack( side= TOP )
link_id_box.set( settings.link_id_val )

connect_btn = Button(root, text=" Connect ", command=login_to)
connect_btn.pack( side= TOP )

master = Frame(root, name='master')
master.pack(fill=BOTH)


# quit if the window is deleted
def close_wnd():
    try:
        logic.close_dr()
    except:
        pass

    master.quit()
    return

root.protocol("WM_DELETE_WINDOW", close_wnd)

nb = Notebook(master, name='nb') # create Notebook in "master"
nb.pack(fill=BOTH, padx=2, pady=3) # fill "master" but pad sides

# Buy items
master_buy = Frame(nb, name='master-buy')

row_cntr = 0

Label( master_buy, text='Buy percent: ' ).grid(row=row_cntr, column=0, sticky="e")

buy_percent_box = StringVar()
Entry( master_buy, width=10, textvariable = buy_percent_box).grid(row=row_cntr, column=1)
buy_percent_box.set( str(settings.buy_percent_val) )

Button(master_buy, text=" Place buy orders", command=buy).grid(row=row_cntr, column=2, padx=20, rowspan = 2, columnspan = 2, ipadx = 5, ipady = 5)
row_cntr += 1


Label( master_buy, text='Buy lower percent: ' ).grid(row=row_cntr, column=0, sticky="e")

buy_lower_percent_box = StringVar()
Entry( master_buy, width=10, textvariable = buy_lower_percent_box).grid(row=row_cntr, column=1)
buy_lower_percent_box.set( str(settings.buy_lower_percent_val) )
row_cntr += 1

Label( master_buy, text='Buy quantity: ' ).grid(row=row_cntr, column=0, sticky="e")

buy_qty_box = StringVar()
Entry( master_buy, width=10, textvariable = buy_qty_box).grid(row=row_cntr, column=1)
buy_qty_box.set( str(settings.buy_qty_val) )
row_cntr += 1


Label( master_buy, text='Min price in RUB: ' ).grid(row=row_cntr, column=0, sticky="e")

min_price_box = StringVar()
Entry( master_buy, width=10, textvariable = min_price_box).grid(row=row_cntr, column=1)
min_price_box.set( str(settings.min_price_val) )

Label(master_buy, text='(buy from current page)' ).grid(row=3, column=2, columnspan = 2, padx=0)
row_cntr += 1

Label( master_buy, text='Max price in RUB: ' ).grid(row=row_cntr, column=0, sticky="e")

max_price_box = StringVar()
Entry( master_buy, width=10, textvariable = max_price_box).grid(row=row_cntr, column=1)
max_price_box.set( str(settings.max_price_val) )

Button(master_buy, text=" Buy fcp ", command=buy_fcp).grid(row=4, column=2, padx=20)
row_cntr += 1

Label( master_buy, text='Min quantity: ' ).grid(row=row_cntr, column=0, sticky="e")

min_qty_box = StringVar()
Entry( master_buy, width=10, textvariable = min_qty_box).grid(row=row_cntr, column=1)
min_qty_box.set( str(settings.min_qty_val) )
row_cntr += 1


Label( master_buy, text='Start page: ' ).grid(row=row_cntr, column=0, sticky="e")

start_page_box = StringVar()
Entry( master_buy, width=10, textvariable = start_page_box).grid(row=row_cntr, column=1, pady = 10)
start_page_box.set( str(settings.start_page_val) )
row_cntr += 1

Label( master_buy, text='--== CSGO: ==--' ).grid(row=row_cntr, column=0, columnspan = 2)
row_cntr += 1


vert = row_cntr
hor = 0
for x in range(len(settings.tags_csgo)):
    settings.tags_csgo[x][2] = IntVar()
    l = Checkbutton(master_buy, text=settings.tags_csgo[x][1], variable=settings.tags_csgo[x][2])
    l.grid(row=vert, column=hor, sticky="w")
    hor += 1
    if ((x+1)%5 == 0):
        vert += 1
        hor = 0
        
vert = vert+1
Label( master_buy, text='--== Artifact: ==--' ).grid(row=vert, column=0, columnspan = 2)

hor = 0
vert = vert+1

for x in range(len(settings.tags_artifact)):
    settings.tags_artifact[x][2] = IntVar()
    l = Checkbutton(master_buy, text=settings.tags_artifact[x][1], variable=settings.tags_artifact[x][2])
    l.grid(row=vert, column=hor, sticky="w")
    hor += 1
    if ((x+1)%5 == 0):
        vert += 1
        hor = 0
        
vert = vert+1
Label( master_buy, text='--== Dota: ==--' ).grid(row=vert, column=0, columnspan = 2)

hor = 0
vert = vert+1

for x in range(len(settings.tags_dota)):
    settings.tags_dota[x][2] = IntVar()
    l = Checkbutton(master_buy, text=settings.tags_dota[x][1], variable=settings.tags_dota[x][2])
    l.grid(row=vert, column=hor, sticky="w")
    hor += 1
    if ((x+1)%5 == 0):
        vert += 1
        hor = 0

vert = vert+1
Label( master_buy, text='--== Team Fort: ==--' ).grid(row=vert, column=0, columnspan = 2)

hor = 0
vert = vert+1

for x in range(len(settings.tags_tf)):
    settings.tags_tf[x][2] = IntVar()
    l = Checkbutton(master_buy, text=settings.tags_tf[x][1], variable=settings.tags_tf[x][2])
    l.grid(row=vert, column=hor, sticky="w")
    hor += 1
    if ((x+1)%5 == 0):
        vert += 1
        hor = 0
        

nb.add(master_buy, text=" Buy ") # add tab to Notebook



# Check items

master_check = Frame(master, name='master-check')
row_cntr = 0

Label( master_check, text='If buy price below this percent order will be cancelled' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")

Button(master_check, text=" Check buy orders ", command=check).grid(row=row_cntr, column=2, padx=20, rowspan = 2, columnspan = 2, ipadx = 5, ipady = 5)
row_cntr += 1

Label( master_check, text='Min check percent: ' ).grid(row=row_cntr, column=0, sticky="e")

check_min_percent_box = StringVar()
Entry( master_check, width=10, textvariable = check_min_percent_box).grid(row=row_cntr, column=1, padx = 30)
check_min_percent_box.set( str(settings.check_min_percent_val) )


row_cntr += 1

Label( master_check, text='If buy price higher this percent order will be cancelled' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
Button(master_check, text=" Delete all buy orders ", command=cancel_all).grid(row=row_cntr, column=2, padx=20)

row_cntr += 1

Label( master_check, text='Max check percent: ' ).grid(row=row_cntr, column=0, sticky="e")

check_percent_box = StringVar()
Entry( master_check, width=10, textvariable = check_percent_box).grid(row=row_cntr, column=1, padx = 30)
check_percent_box.set( str(settings.check_max_percent_val) )

Button(master_check, text=" Calc buy orders ", command=calc_buy_orders).grid(row=row_cntr, column=2, padx=20)

row_cntr += 1

Label( master_check, text='Start checking from № in order list' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
row_cntr += 1

Label( master_check, text='Check/Del from N: ' ).grid(row=row_cntr, column=0, sticky="e")

check_start_from_box = StringVar()
Entry( master_check, width=10, textvariable = check_start_from_box).grid(row=row_cntr, column=1, padx = 30)
check_start_from_box.set( str(settings.check_start_from_val) )
row_cntr += 1

Label( master_check, text='Stop checking to № in order list' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
row_cntr += 1

Label( master_check, text='Check/Del to N: ' ).grid(row=row_cntr, column=0, sticky="e")

check_end_from_box = StringVar()
Entry( master_check, width=10, textvariable = check_end_from_box).grid(row=row_cntr, column=1, padx = 30)
check_end_from_box.set( str(settings.check_end_from_val) )
row_cntr += 1



nb.add(master_check, text=" Check ")



# Sell items
master_sell = Frame(master, name='master-sell')

row_cntr = 0

Label( master_sell, text='Game to sell: ' ).grid(row=row_cntr, column=0, sticky="e")
row_cntr += 1

vert = row_cntr
hor = 0
for x in range(len(settings.sell_games_ids)):
    is_on = settings.sell_games_ids[x][2]
    settings.sell_games_ids[x][2] = IntVar()
    l = Checkbutton(master_sell, text=settings.sell_games_ids[x][1], variable=settings.sell_games_ids[x][2])
    settings.sell_games_ids[x][2].set(is_on)
    
    l.grid(row=vert, column=hor, sticky="w")
    hor += 1
    if ((x+1)%3 == 0):
        vert += 1
        hor = 0

Button(master_sell, text=" Sell items ", command=sell).grid(row=row_cntr, column=3, padx=20, rowspan = 2, columnspan = 2, ipadx = 5, ipady = 5)
row_cntr = vert+1
        
        
Label( master_sell, text='Percent to detemine if someone selling item too low' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
row_cntr += 1

Label( master_sell, text='than others. If so im selling higher than him' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
Button(master_sell, text=" Cancel all sell orders ", command=sell_cancel).grid(row=row_cntr, column=3, padx=20)
row_cntr += 1

Label( master_sell, text='Gap percent: ' ).grid(row=row_cntr, column=0, sticky="e")

gap_percent_box = StringVar()
Entry( master_sell, width=10, textvariable = gap_percent_box).grid(row=row_cntr, column=1)
gap_percent_box.set( str(settings.gap_percent_val) )
Button(master_sell, text=" Resell items ", command=resell).grid(row=row_cntr, column=3, padx=20)
row_cntr += 1

Label( master_sell, text='How lower to place order from best sell price' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
Button(master_sell, text="Cancel all confirming", command=cancel_confirming).grid(row=row_cntr, column=3, padx=20)
row_cntr += 1

Label( master_sell, text='Place lower in RUB: ' ).grid(row=row_cntr, column=0, sticky="e")

place_lower_box = StringVar()
Entry( master_sell, width=10, textvariable = place_lower_box).grid(row=row_cntr, column=1)
place_lower_box.set( str(settings.place_lower_val) )
row_cntr += 1

Label( master_sell, text='My sell order must be in N first orders when reselling' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
row_cntr += 1

Label( master_sell, text='Resell lot position: ' ).grid(row=row_cntr, column=0, sticky="e")

resell_lot_position_box = StringVar()
Entry( master_sell, width=10, textvariable = resell_lot_position_box).grid(row=row_cntr, column=1)
resell_lot_position_box.set( str(settings.resell_lot_position_val) )
row_cntr += 1

Label( master_sell, text='Start reselling from № sell order (NOT USED NOW)' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
row_cntr += 1

Label( master_sell, text='Resell from N: ' ).grid(row=row_cntr, column=0, sticky="e")

sell_start_from_box = StringVar()
Entry( master_sell, width=10, textvariable = sell_start_from_box).grid(row=row_cntr, column=1, padx = 30)
sell_start_from_box.set( str(settings.sell_start_from_val) )
row_cntr += 1

Label( master_sell, text='Stop reselling to № sell order (NOT USED NOW)' ).grid(row=row_cntr, column=0, columnspan=2, sticky="w")
row_cntr += 1

Label( master_sell, text='Resell to N: ' ).grid(row=row_cntr, column=0, sticky="e")

sell_end_from_box = StringVar()
Entry( master_sell, width=10, textvariable = sell_end_from_box).grid(row=row_cntr, column=1, padx = 30)
sell_end_from_box.set( str(settings.sell_end_from_val) )
row_cntr += 1


        
nb.add(master_sell, text=" Sell ")


settings.msg_box = Message( root, bg='white', fg='black', width=400, borderwidth=0 )
settings.msg_box.pack( side=BOTTOM )
settings.msg_box['text'] = '                '
 
Button(root, text=" Cancel last command ", command=cancel_btn).pack( side= BOTTOM )

# start the app
if __name__ == "__main__":
    master.mainloop() # call master's Frame.mainloop() method.
    root.destroy()    # if mainloop quits, destroy window
